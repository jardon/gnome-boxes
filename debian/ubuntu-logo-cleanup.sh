#!/bin/sh

# GNOME Boxes hasn't implemented a way to invalidate the cache
# so we do it manually once to get the new Ubuntu & Fedora logos
rm .cache/gnome-boxes/logos/*.svg
